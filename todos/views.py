from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, CreateTodoItem


# Create your views here.
def todo_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list": list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    tasks = get_object_or_404(TodoList, id=id)
    context = {
        "task_detail": tasks,
    }
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=list)

    context = {
        "list_object": list,
        "todo_form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = CreateTodoItem(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = CreateTodoItem()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = CreateTodoItem(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = CreateTodoItem(instance=list)

    context = {
        "list_object": list,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
