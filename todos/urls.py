from django.urls import path, include
from todos.views import (
    todo_list,
    todo_list_detail,
    create_todo,
    edit_todo,
    todo_list_delete,
    create_item,
    edit_item,
)

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("edit/<int:id>/", edit_todo, name="todo_list_update"),
    path("delete/<int:id>/", todo_list_delete, name="todo_list_delete"),
    path("create_item/", create_item, name="create_item"),
    path("edit_item/<int:id>", edit_item, name="todo_item_update"),
]
